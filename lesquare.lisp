(in-package :lesquare)

(defparameter *width* 1280)
(defparameter *height* 720)
(defparameter *paused* nil)
(defparameter *soundtrack* nil)
(defparameter *grid-directions* '(:right :up :left :down))
(defparameter *grid-diagonals* '(:upright :upleft :downright :downleft))

;;; Controls

(defvar *player-1-joystick* 0)
(defvar *player-2-joystick* nil)
(defvar *player-3-joystick* nil)
(defvar *player-4-joystick* nil)

(defun holding-button-p (joystick)
  (when (numberp joystick)
    (some #'(lambda (button) (joystick-button-pressed-p button joystick))
	  '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20))))

(defun holding-down-arrow () (or (keyboard-down-p :kp2) (keyboard-down-p :down)))
(defun holding-up-arrow () (or (keyboard-down-p :kp8) (keyboard-down-p :up)))
(defun holding-left-arrow () (or (keyboard-down-p :kp4) (keyboard-down-p :left)))
(defun holding-right-arrow () (or (keyboard-down-p :kp6) (keyboard-down-p :right)))
(defun holding-shift-p () (xelf::holding-shift))
(defun holding-space () (keyboard-down-p :space))

(defun arrow-keys-direction ()
  (cond 
    ((and (holding-down-arrow) (holding-right-arrow)) :downright)
    ((and (holding-down-arrow) (holding-left-arrow)) :downleft)
    ((and (holding-up-arrow) (holding-right-arrow)) :upright)
    ((and (holding-up-arrow) (holding-left-arrow)) :upleft)
    ((holding-down-arrow) :down)
    ((holding-up-arrow) :up)
    ((holding-left-arrow) :left)
    ((holding-right-arrow) :right)))

;;; Player variables

(defvar *player-1* nil)
(defvar *player-2* nil)
(defvar *player-3* nil)
(defvar *player-4* nil)

(defun player-1 () *player-1*)
(defun player-2 () *player-2*)
(defun player-3 () *player-3*)
(defun player-4 () *player-4*)

(defun find-player (n)
  (ecase n
    (1 (player-1))
    (2 (player-2))
    (3 (player-3))
    (4 (player-4))))

(defun set-player-1 (x) (setf *player-1* x))
(defun set-player-2 (x) (setf *player-2* x))
(defun set-player-3 (x) (setf *player-3* x))
(defun set-player-4 (x) (setf *player-4* x))

(defun player-1-p (x) (eq (find-object x) (find-object *player-1*)))
(defun player-2-p (x) (eq (find-object x) (find-object *player-2*)))
(defun player-3-p (x) (eq (find-object x) (find-object *player-3*)))
(defun player-4-p (x) (eq (find-object x) (find-object *player-4*)))

;;; Game clock and scoring

(defparameter *score-font* "sans-mono-bold-12")
(defparameter *big-font* "sans-mono-bold-16")

(defconstant +60fps+ 60)
(defconstant +seconds-per-minute+ 60)
(defun seconds (n) (* +60fps+ n))
(defun minutes (n) (* (seconds +seconds-per-minute+) n))

(defparameter *game-length* (minutes 2))

(defvar *game-clock* 0)

(defun reset-game-clock ()
  (setf *game-clock* 0))

(defun update-game-clock ()
  (incf *game-clock*))

(defun game-on-p () (plusp *game-clock*))

(defun game-clock () *game-clock*)

(defun game-clock-string (&optional (clock *game-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

(defvar *score-1* 0)
(defvar *score-2* 0)

(defun reset-score () (setf *score-1* 0 *score-2* 0))

;;; Buffer utils (see below defs for Arena)

(defmethod grid-offset ((buffer buffer)) 0)
(defmethod grid-object-size ((buffer buffer)) (units 1))

;;; Defining the grid-based world

(defparameter *unit* 17)

(defun units (n) (* n *unit*))

(defun grid-position (x y)
  (values (units x) (units y)))

(defun grid-position-center (x y)
  (values (cfloat (+ (units x) (units 0.5)))
	  (cfloat (+ (units y) (units 0.5)))))

(defun grid-position-bounding-box (x y)
  ;; top left right bottom
  (values (units y) 
	  (units x) 
	  (units (1+ x)) 
	  (units (1+ y))))

(defparameter *margin-width* 0.5)

(defun grid-object-bounding-box (x y)
  (values (+ (units y) *margin-width*)
	  (+ (units x) *margin-width*)
	  (- (units (1+ x)) *margin-width*)
	  (- (units (1+ y)) *margin-width*)))

;;; Level progression and difficulty

(defparameter *level* 0)

(defun by-level (&rest args)
  (if (<= (length args) *level*)
      (nth (- (length args) 1) args)
      (nth *level* args)))

(defparameter *difficulty* 0)

(defun with-difficulty (&rest args)
  (if (<= (length args) *difficulty*)
      (nth (1- (length args)) args)
      (nth *difficulty* args)))

;;; Colors

(defparameter *neutral-color* "white")
;; (defparameter *pond-color* "yellow green")

(defparameter *traditional-player-colors* 
  '("gold" "olive drab" "RoyalBlue3" "dark orchid"))
(defparameter *pastel-player-colors* 
  '("yellow" "pale turquoise" "pink" "pale green"))

(defparameter *player-1-color* "gold")
(defparameter *player-2-color* "olive drab")
(defparameter *player-3-color* "RoyalBlue3")
(defparameter *player-4-color* "dark orchid")

(defparameter *two-color-themes* 
  '((:snefru "dark slate blue" "green" 
     "magenta" "cyan")
    (:xalcrys "black" "blue violet" 
     "deep pink" "cornflower blue")
    (:zupro "olive drab" "hot pink" 
     "cyan" "yellow")))

(defparameter *three-color-themes*
  '((:snafu "dark magenta" "gray20" 
     "cyan" "red" "yellow")
    (:atlantis "midnight blue" "dodger blue" 
     "pale green" "hot pink" "red")
    (:radium "dark olive green" "gold"
     "cyan" "chartreuse" "magenta")
    (:krez "dark orchid" "maroon2" 
     "green" "red" "yellow")))

(defparameter *four-color-themes*
  '((:zerk "black" "gray40" 
     "maroon2" "yellow green" "orange" "cornflower blue")
    (:tandy "DarkSlateBlue" "gray80" 
     "yellow" "green" "cyan" "deep pink")
    (:zendium "gray17" "orchid"
     "deep pink" "deep sky blue" "chartreuse" "orange")
    (:command "dim gray" "yellow" 
     "cyan" "deep pink" "red" "green yellow")))

(defvar *red-green-color-blindness* nil)

(defparameter *red-green-color-blindness-theme* 
  '("cornflower blue" "yellow" "pale green" "violet red"))

(defun red-green-color-blindness-theme (&optional (colors 4))
  (append (list "black" "gray50")
	  (subseq (derange *red-green-color-blindness-theme*) 
		  0 colors)))

(defresource "hash1.png")
(defresource "hash2.png")
(defresource "hash3.png")
(defresource "hash4.png")

(defun ball-hash-image (n)
  (ecase n
    (1 "hash1.png")
    (2 "hash2.png")
    (3 "hash3.png")
    (4 "hash4.png")
    (5 "hash1.png")
    (6 "hash2.png")
    (7 "hash3.png")
    (8 "hash4.png")))

(defresource "gate1-hash1.png")
(defresource "gate1-hash2.png")
(defresource "gate1-hash3.png")
(defresource "gate1-hash4.png")
(defresource "gate2-hash1.png")
(defresource "gate2-hash2.png")
(defresource "gate2-hash3.png")
(defresource "gate2-hash4.png")
(defresource "brick-hash1.png")
(defresource "brick-hash2.png")
(defresource "brick-hash3.png")
(defresource "brick-hash4.png")

(defun hash-image (n)
  (if *red-green-color-blindness*
      (ecase n
	(1 "gate1-hash1.png")
	(2 "gate1-hash2.png")
	(3 "gate1-hash3.png")
	(4 "gate1-hash4.png"))
      "gate.png"))

(defun large-hash-image (n)
  (if *red-green-color-blindness*
      (ecase n
	(1 "gate2-hash1.png")
	(2 "gate2-hash2.png")
	(3 "gate2-hash3.png")
	(4 "gate2-hash4.png"))
      "gate2.png"))

(defun solid-hash-image (n)
  (or (when *red-green-color-blindness*
	(case n
	  (1 "brick-hash1.png")
	  (2 "brick-hash2.png")
	  (3 "brick-hash3.png")
	  (4 "brick-hash4.png")))
      "gate3.png"))

(defparameter *boss-theme* '(:voltz "black" "gray30" "orchid" "medium orchid" "dark orchid" "deep pink" "green yellow"))

(defparameter *themes* 
  (append *two-brick-themes* *three-brick-themes*
	  *four-brick-themes* (list *boss-theme*)))

(defun find-theme (name)
  (rest (assoc name *themes*)))

(defparameter *theme* '("black" "white" "red" "blue"))

(defun theme-colors (&optional (theme *theme*))
  (rest (rest theme)))

(defun color-hash (color)
  (let ((pos (position color (theme-colors) :test 'equal)))
    (when pos (1+ pos))))

(defun set-theme (&optional (theme :wizard))
  (setf *theme* 
	(if (consp theme) 
	    theme
	    (find-theme theme))))

(defun random-theme () (random-choose (mapcar #'car *themes*)))

(defun set-random-theme () (set-theme (random-theme)))

(defun background-color ()
  (when *theme* (first *theme*)))

(defun wall-color ()
  (when *theme* (second *theme*)))

(defun level-colors ()
  (when *theme* (rest (rest *theme*))))

(defparameter *levels* 
  '((:difficulty 0 :colors 2 :hazards nil :wildcards nil)))

(defun nth-level (level)
  (nth (mod level (length *levels*)) *levels*))

(defun level-difficulty (&optional (level *level*))
  (getf (nth-level level) :difficulty))

(defun level-music (&optional (level *level*))
  (or (getf (nth-level level) :music)
      *soundtrack*))

(defun level-theme (&optional (level *level*))
  (let ((ncolors (getf (nth-level level) :colors)))
    (random-choose 
     (mapcar #'car
	     (ecase ncolors
	       (2 *two-brick-themes*)
	       (3 *three-brick-themes*)
	       (4 *four-brick-themes*)
	       (5 '((:voltz))))))))

(defun level-hazards (&optional (level *level*))
  (getf (nth-level level) :hazards))

(defun level-wildcards (&optional (level *level*))
  (getf (nth-level level) :wildcards))

;;; Base class for objects in lesquare

(defparameter *slide-frames* 10)

(defclass thing (xelf:node)
  ((grid-x :initform 0 :accessor grid-x :initarg :grid-x)
   (grid-y :initform 0 :accessor grid-y :initarg :grid-y)
   (color :initform *neutral-color* :accessor color :initarg :color)
   (speed :initform 0)
   (heading :initform 0.0)
   (direction :initform :right :accessor direction :initarg :direction)
   ;; netplay 
   (player-id :initform nil)
   (input-p :initform nil :accessor input-p)
   (input-update-p :initform nil :accessor input-update-p)
   (input-direction :initform nil :accessor input-direction)
   (input-jumping-p :initform nil :accessor input-jumping-p)
   ;; slide movement
   (gx0 :initform 0)
   (gy0 :initform 0)
   (px :initform 0)
   (py :initform 0)
   (pz :initform 0)
   (x0 :initform 0)
   (y0 :initform 0)
   (z0 :initform 0)
   (origin-gx :initform 0)
   (origin-gy :initform 0)
   (interpolation :initform :linear)
   (frames :initform 0)
   (slide-frames :initform *slide-frames*)
   (slide-timer :initform 0)))

(defun find-heading-direction (float)
  (or (heading-direction float) :left))

(defmethod resize-to-grid ((thing thing))
  (resize thing (grid-object-size (current-buffer)) (grid-object-size (current-buffer))))

(defmethod initialize-instance :after ((thing thing) &key)
  (resize-to-grid thing))

(defmethod move-to-grid ((thing thing) gx gy)
  (assert (and (integerp gx) (integerp gy)))
  (with-slots (height width grid-x grid-y gx0 gy0) thing
    (multiple-value-bind (x y) (grid-position gx gy)
      (setf grid-x gx grid-y gy)
      (move-to thing 
	       (+ x (grid-offset (current-buffer)))
	       (+ y (grid-offset (current-buffer)))
	       (setf gx0 gx gy0 gy)))))

(defun place (thing x y)
  (with-buffer (current-buffer)
    (add-node (current-buffer) thing)
    (move-to-grid thing x y)))

(defmethod center-on ((thing thing) cx cy)
  (with-slots (height width) thing
    (move-to thing (- cx (/ width 2)) (- cy (/ height 2)))))

(defmethod spawn-position ((thing thing))
  (values (/ *width* 2) (/ *height* 2)))

(defmethod spawn ((thing thing))
  (multiple-value-bind (x y) (spawn-position thing)
    (center-on thing x y)))

;;; Sliding movement 

(defmethod sliding-p ((thing thing))
  (plusp (field-value 'slide-timer thing)))

(defun interpolate (px x a &optional (interpolation :linear))
  (let ((range (- px x)))
    (float 
     (+ px
	(ecase interpolation
	  (:linear (* a range))
	  (:sine (* (sin (* a (/ pi 2))) range)))))))

(defmethod slide-to ((self thing) x1 y1 &key (interpolation :linear) frames)
  (with-slots (x y px py x0 y0 interpolation slide-timer slide-frames) self
    (setf px x py y)
    (setf x0 x1 y0 y1)
    (setf interpolation interpolation)
    (let ((timer (or frames slide-frames)))
      (setf slide-timer timer frames timer))))

(defmethod slide-to-grid ((thing thing) gx gy &key (interpolation :linear) frames)
  (multiple-value-bind (cx cy) (grid-position-center gx gy)
    (with-slots (grid-x grid-y gx0 gy0 origin-gx origin-gy height width slide-frames) thing
      (setf gx0 gx gy0 gy)
      (setf origin-gx grid-x origin-gy grid-y) 
      (let ((timer (or frames slide-frames)))
	(slide-to thing (- cx (/ width 2)) (- cy (/ height 2)) :interpolation interpolation :frames timer)))))

(defmethod update-slide ((self thing))
  (with-slots (x0 y0 px py slide-frames slide-timer interpolation) self
    (unless (zerop slide-timer)
      ;; scale to [0,1]
      (let ((a (/ (- slide-timer slide-frames) slide-frames)))
	(move-to self
		 (interpolate px x0 a interpolation)
		 (interpolate py y0 a interpolation))
	(decf slide-timer)))))

(defmethod cancel-slide ((thing thing))
  (setf (slot-value thing 'slide-timer) 0))

(defmethod snap-to-grid ((thing thing))
  (with-slots (gx0 gy0) thing
    (move-to-grid thing gx0 gy0)))

(defmethod fuzz-to-grid ((thing thing))
  (with-slots (x y) thing
    (let ((tolerance 3.0)
	  (gx (round (/ x (units 1))))
	  (gy (round (/ y (units 1)))))
      (if (< tolerance (distance x y (* *unit* gx) (* *unit* gy)))
	  (message "Warning: could not fuzzy-match ~S for grid position ~S." thing (list gx gy 'from x y))
	  (move-to-grid thing gx gy)))))

(defmethod restore-from-slide ((thing thing))
  (with-slots (origin-gx origin-gy) thing
    (move-to-grid thing origin-gx origin-gy)
    (snap-to-grid thing)))

;;; AI basics

(defmethod think ((thing thing)) nil)

(defmethod should-think-p ((thing thing))
  (not (sliding-p thing)))

(defmethod arrive ((thing thing)) 
  (snap-to-grid thing)
  (when (should-think-p thing)
    (think thing)))

(defmethod update :before ((thing thing))
  (if (sliding-p thing)
      (progn (update-slide thing)
	     ;; did we arrive?
	     (when (not (sliding-p thing))
	       (with-slots (grid-x gx0 grid-y gy0) thing
		 (setf grid-x gx0 grid-y gy0)
		 (arrive thing))))
      (when (should-think-p thing)
	(think thing))))

(defmethod cursor ((thing thing)) (player-1))

(defmethod distance-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (distance cx cy px py))))

(defmethod heading-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (find-heading cx cy px py))))

(defmethod direction-to-cursor ((thing thing))
  (or (heading-direction (heading-to-cursor thing)) :right))

;;; Basic movement and facing

(defmethod update-heading ((thing thing))
  (with-slots (heading direction) thing
    (setf heading (direction-heading direction))))

(defmethod face ((thing thing) direction)
  (setf (direction thing) direction)
  (update-heading thing))

(defmethod move-grid ((thing thing) direction &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (move-to-grid thing x y))))

(defmethod slide ((thing thing) direction &optional (distance 1) frames)
  (with-slots (grid-x grid-y slide-frames) thing
    (assert (integerp slide-frames))
    (assert (keywordp direction))
    (assert (integerp distance))
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (slide-to-grid thing x y :frames (or frames slide-frames)))))

(defmethod move-forward ((thing thing) &optional (distance 1))
  (with-slots (direction) thing
    (move-grid thing direction distance)))

(defmethod slide-forward ((thing thing) &optional (distance 1))
  (with-slots (direction slide-frames) thing
    (assert (integerp slide-frames))
    (assert (keywordp direction))
    (assert (integerp distance))
    (slide thing direction distance slide-frames)))

(defmethod turn-around ((thing thing))
  (face thing (opposite-direction (direction thing))))

(defmethod turn-leftward ((thing thing))
  (face thing (xelf::left-turn (xelf::left-turn (direction thing)))))

(defmethod turn-rightward ((thing thing))
  (face thing (xelf::right-turn (xelf::right-turn (direction thing)))))

(defmethod find-color ((thing thing))
  (slot-value thing 'color))

(defmethod paint ((thing thing) color)
  (setf (slot-value thing 'color) color))

(defun same-color-p (a b)
  (string= (find-color a) (find-color b)))

(defmethod draw ((self thing))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 ;; apply shading
				 :vertex-color color
				 :blend :alpha
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading))))))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod center-of-arena ()
  (values (/ *width* 2) (/ *height* 2)))

(defmethod heading-to-center ((thing thing))
  (multiple-value-bind (tx ty) (center-point thing)
    (multiple-value-bind (cx cy) (center-of-arena)
      (find-heading tx ty cx cy))))
  
(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun decay (x)
  (let ((z (* 0.94 x)))
    z))

;;; Colored trails

(defclass trail (thing)
  ((color :initform "blue")))

(defmethod initialize-instance :after ((trail trail) &key (color "blue"))
  (setf (slot-value trail 'color) 
	(or (pen-color) color)))

(defmethod entry-args ((trail trail))
  (list :color (slot-value trail 'color)))

(defmethod draw ((trail trail))
  (with-fields (grid-x grid-y color) trail
    (multiple-value-bind (top left right bottom)
	(grid-position-bounding-box grid-x grid-y)
      (draw-box left top (- right left) (- bottom top) :color color))))

(defmethod collide ((trail trail) (ball ball))
  (paint ball (find-color trail))
  (bounce ball)
  (destroy trail))

(defmethod draw-trails (x y width height color)
  (let (trails)
    (dotimes (i width)
      (dotimes (j height)
	(let ((trail (make-instance 'trail :color color)))
	  (push trail trails)
	  (add-node (current-buffer) trail)
	  (move-to-grid trail (+ x i) (+ y j)))))
    trails))

;;; Walls

(defparameter *wall-color* "gray20")

(defclass wall (trail)
  ((color :initform *wall-color*)))

(defmethod draw ((wall wall))
  (with-fields (grid-x grid-y) wall
    (draw-box (* *unit* grid-x) (* *unit* grid-y) *unit* *unit* :color (wall-color))))

(defmethod collide ((ball ball) (wall wall)) nil)
;;  (bounce ball))

(defmethod collide ((wall wall) (ball ball))
  (bounce ball))

;;; The player avatar

(defparameter *player-1-image* "robot.png")
(defparameter *player-2-image* "robot.png")

(defclass player (thing)
  ((image :initform *player-1-image*)
   (deadp :initform nil :accessor deadp)
   (slide-frames :initform 7)
   (paint-color :initform "white" :accessor paint-color)))

(defmethod humanp ((player player)) nil)

(defmethod update :before ((player player))
  (with-slots (grid-x grid-y) player
    (when (and (zerop grid-x) (zerop grid-y))
      (message "Dropping misplaced object ~S" player)
      (move-to-grid player 2 2))))

(defmethod kill ((player player))
  (when (not (deadp player))
    (setf (deadp player) t)
    (play-sample "death.wav")
    (play-sample "death-alien.wav")))

(defmethod paint ((player player) color)
  (setf (paint-color player) color)) 

(defmethod collide ((trail trail) (player player))
  (cancel-slide player)
  (restore-from-slide player))

(defmethod holding-fire ((player player)) nil)
(defmethod holding-direction ((player player)) nil)

(defmethod update ((player player))
  (unless (sliding-p player)
    (let ((dir (holding-direction player)))
      (when dir
	(face player dir))
      (slide player dir 1))))

(defmethod update :around ((player player))
  (unless (deadp player)
    (call-next-method)))

;;; AI planning

(defmethod find-waypoint ((player player) target)
  (center-point target))

(defmethod find-target ((player player)) nil)

(defmethod waypoint ((player player))
  (find-waypoint player (find-target player)))

(defun jitter (heading)
  (+ heading (* (if (difficult-p) 0.15 0.2) (sin (/ *updates* 24)))))

;;; Control methods

(defmethod joystick-number ((player player)) nil)

(defmethod joystick-connected-p ((player player)) 
  (numberp (joystick-number player)))

(defmethod stick-direction ((player player))
  (when (humanp player)
    (let ((joystick (joystick-number player)))
      (when joystick 
	(when (left-analog-stick-pressed-p joystick)
	  (find-heading-direction (left-analog-stick-heading joystick)))))))

(defmethod stick-heading ((player player))
  (let ((dir (stick-direction player)))
    (when dir (direction-heading dir))))

;;; First player uses keyboard (or joystick 1)

(defclass player-1 (player) 
  ((color :initform *player-1-color*)
   (player-id :initform 1)))

(defmethod holding-fire ((player player-1))
  (or (holding-shift) (holding-button :throw)))

(defmethod holding-direction ((player player-1))
  (cond ((or (holding-down-arrow) (holding-button :down)) :down)
	((or (holding-up-arrow) (holding-button :up)) :up)
	((or (holding-left-arrow) (holding-button :left)) :left)
	((or (holding-right-arrow) (holding-button :right)) :right)))

(defparameter *player-1-human-p* nil)

(defmethod humanp ((self player-1)) *player-1-human-p*)

(defmethod stick-direction :around ((frog player-1))
  (if *player-1-human-p* 
      (or (arrow-keys-direction) (call-next-method))
      (call-next-method)))

(defmethod jumping-p :around ((frog player-1))
  (if *player-1-human-p* 
      (or (holding-shift-p) (call-next-method))
      (call-next-method)))

;;; Second player uses joystick

(defclass player-2 (player)
  ((color :initform *player-2-color*)
   (player-id :initform 2)))

(defmethod holding-fire ((player player-2))
  (holding-button :throw :joystick *player-2-joystick* :config *player-2-config*))

(defmethod holding-direction ((player player-2))
  (cond ((holding-button :down :joystick *player-2-joystick* :config *player-2-config*) :down)
	((holding-button :up :joystick *player-2-joystick* :config *player-2-config*) :up)
	((holding-button :left :joystick *player-2-joystick* :config *player-2-config*) :left)
	((holding-button :right :joystick *player-2-joystick* :config *player-2-config*) :right)))

;;; Players 3 and 4 

(defclass player-3 (player) 
  ((color :initform *player-3-color*)
   (player-id :initform 3)))

(defclass player-4 (player) 
  ((color :initform *player-4-color*)
   (player-id :initform 4)))

(defmethod joystick-number ((player player-1)) *player-1-joystick*)
(defmethod joystick-number ((player player-2)) *player-2-joystick*)
(defmethod joystick-number ((player player-3)) *player-3-joystick*)
(defmethod joystick-number ((player player-4)) *player-4-joystick*)

(defmethod x-disp ((player player-1)) (units 2))
(defmethod y-disp ((player player-1)) (units 3))
(defmethod x-disp ((player player-2)) (units 3))
(defmethod y-disp ((player player-2)) (units 2))
(defmethod x-disp ((player player-3)) (units -3))
(defmethod y-disp ((player player-3)) (units -2))
(defmethod x-disp ((player player-4)) (units -2))
(defmethod y-disp ((player player-4)) (units -3))

(defmethod find-opponents ((player player-1)) (list (player-2) (player-3) (player-4)))
(defmethod find-opponents ((player player-2)) (list (player-1) (player-3) (player-4)))
(defmethod find-opponents ((player player-3)) (list (player-1) (player-2) (player-4)))
(defmethod find-opponents ((player player-4)) (list (player-1) (player-2) (player-3)))

;;; Apply shading/detail to the player(s) and show team color

(defmethod draw :after ((player player))
  (with-fields (color heading kick-clock paint-color) player
    (multiple-value-bind (top left right bottom)
	(bounding-box player)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture "robot-detail.png")
				 :vertex-color color
				 :angle (+ 90 (heading-degrees heading)))
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture "robot-swatch.png")
				 :vertex-color paint-color
				 :angle (+ 90 (heading-degrees heading))))))

;;; Can't go thru walls

(defmethod collide ((wall wall) (player player))
  (cancel-slide player)
  (restore-from-slide player))

;;; The arena

(defclass arena (xelf:buffer)
  ((quadtree-depth :initform 9)))

(defmethod grid-offset ((arena arena)) *margin-width*)

(defmethod grid-object-size ((arena arena))
  (- (units 1) (* 2 *margin-width*)))

(defun drop-wall (gx gy)
  (let ((wall (make-instance 'wall)))
    (add-node (current-buffer) wall)
    (move-to-grid wall gx gy)))

(defun draw-horizontal-wall (gx gy length)
  (dotimes (n length)
    (drop-wall (+ n gx) gy)))

(defun draw-vertical-wall (gx gy length)
  (dotimes (n length)
    (drop-wall gx (+ n gy))))

(defun draw-border-wall (gx gy width height)
  (draw-horizontal-wall 0 0 width)
  (draw-horizontal-wall 0 (- height 1) width)
  (draw-vertical-wall 0 1 (- height 2))
  (draw-vertical-wall (- width 1) 1 (- height 2)))

(defun turnstile (gx gy) 
  (draw-horizontal-wall (+ gx 1) (+ gy 3) 3)
  (draw-horizontal-wall (+ gx 3) (+ gy 3) 3)
  (draw-vertical-wall (+ gx 3) (+ gy 1) 2)
  (draw-vertical-wall (+ gx 3) (+ gy 4) 2))
  
(defmethod quit-game ((arena arena))
  (quit))

;;; Puzzle generation

(defun width-in-units () (truncate (/ *width* *unit*)))
(defun height-in-units () (truncate (/ *height* *unit*)))

(defun first-color (x y)
  (draw-trails-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (first (theme-colors))))

(defun second-color (x y)
  (draw-trails-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (second (theme-colors))))

(defun third-color (x y)
  (draw-trails-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (third (theme-colors))))

(defun fourth-color (x y)
  (draw-trails-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (fourth (theme-colors))))

(defun courtyard (x y)
  (draw-courtyard x y 7 7))

(defun expand-entries (entries)
  (mapcar #'expand entries))

(defun row-entries (row)
  (apply #'lined-up (mapcar #'item-entries row)))

(defun template-entries (template)
  (apply #'stacked-up (mapcar #'row-entries template)))

(defmethod initialize-instance :after ((arena arena) &key)
  (with-buffer arena
    (setf *arena* arena)
    (resize arena *width* *height*)
    (draw-border-wall 0 0 (width-in-units) (height-in-units))
    (bind-event arena '(:escape) 'setup)
    (bind-event arena '(:r :control) 'reset-game)
    (bind-event arena '(:p :control) 'pause)
    (bind-event arena '(:q :control) 'quit-game)
    (set-player-1 (make-instance 'player-1))
    (set-player-2 (make-instance 'player-2))
    (set-player-3 (make-instance 'player-3))
    (set-player-4 (make-instance 'player-4))
    (do-nodes (node arena)
      (spawn node))))

(defun do-reset ()
  (dotimes (n 64) 
    (halt-sample n))
  (reset-score)
  (start (make-instance 'arena))
  (reset-game-clock))

(defmethod reset-game ((arena arena))
  (stop arena)
  (setf *level* 0)
  (do-reset)
  (at-next-update (destroy arena)))

(defmethod next-level ((arena arena))
  (stop arena)
  (incf *level*)
  (do-reset)
  (at-next-update (destroy arena)))

(defmethod retry-level ((arena arena))
  (stop arena)
  (do-reset)
  (at-next-update (destroy arena)))

(defmethod pause ((arena arena))
  (setf *paused* (if *paused* nil t)))

(defmethod update :around ((arena arena))
  (when (not *paused*)
    (update-game-clock)
    (call-next-method)))

(defmethod draw :before ((arena arena))
  (draw-box 0 0 *width* *height* :color (background-color)))

(defun status-line-string ()
  (if (not *paused*)
      "[Arrows] Move     [Ctrl-R] reset     [Escape key] Setup     [Ctrl-P] pause    [Ctrl-Q] quit"
      "Game is paused. Press [Control-P] again to resume game, or [Control-Q] to quit."))

(defmethod draw :after ((arena arena))
  (draw-box 0 (- *height* 20) *width* 20 :color "gray20")
  (let ((color (paint-color (player-1))))
    (when (not *paused*)
      (draw-string (format nil " Level ~S" (1+ *level*)) 
		   (units 7.8) (- *height* 17)
		   :color "yellow"
		   :font *score-font*)
      (draw-box (units 0.2) (- *height* 16) 22 12 :color color)
      (draw-string (string-capitalize color)
		   (units 2.1) (- *height* 17)
		   :color color
		   :font *score-font*)
      (draw-string (game-clock-string) 
		   (units 11.9) (- *height* 17)
		   :color "cyan"
		   :font *score-font*))
    (draw-string (status-line-string)
		 (units 14.2) (- *height* 17)
		 :color "white"
		 :font *score-font*))
  (do-nodes (node arena)
    (when (or (typep node (find-class 'enemy))
	      (typep node (find-class 'player)))
      (draw node))))

;;; Preventing mousey movements

(defmethod handle-point-motion ((self arena) x y))
(defmethod press ((self arena) x y &optional button))
(defmethod release ((self arena) x y &optional button))
(defmethod tap ((self arena) x y))
(defmethod alternate-tap ((self arena) x y))

;;; Main program

(defparameter *title-string* "le square 0.1")

(defun lesquare (&optional (level 1))
  (set-theme (random-choose (mapcar #'car *three-trail-themes*)))
  (setf *player-1-joystick* nil)
  (setf *player-2-joystick* nil)
  (setf *player-3-joystick* nil)
  (setf *player-4-joystick* nil)
  (setf *paused* nil)
  (setf *level* (1- level))
  (setf *fullscreen* nil)
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :linear)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *frame-rate* 60)
  (disable-key-repeat) 
  (with-session 
    (open-project "lesquare")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (at-next-update (switch-to-buffer (make-instance 'arena)))))

;;; lesquare.lisp ends here
